package dv.spring.kotlin.entity

data class ShoppingCart(var shoppingCartStatus: ShoppingCartStatus = ShoppingCartStatus.WAIT){
    var selectedProduct = mutableListOf<SelectedProduct>()
    var shippingAddress = mutableListOf<Address>()
    lateinit var customer : Customer
}