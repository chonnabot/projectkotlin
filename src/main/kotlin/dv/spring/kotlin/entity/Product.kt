package dv.spring.kotlin.entity


data class Product(var name: String,
                   var description: String,
                   var price: Double,
                   var amountInStock: Int,
                   var imageUtil: String?=null){
    lateinit var manufacturer: Manufacturer
    constructor( name: String,
                 description: String,
                 price: Double,
                 amountInStock: Int,
                 manufacturer: Manufacturer,
                 imageUrl: String?):
            this(name,description,price,amountInStock,imageUrl){
        this.manufacturer = manufacturer
    }
}