package dv.spring.kotlin.entity

data class Manufacturer(var name: String,
                        var telNo: String) {
    var product = mutableListOf<Product>()
    lateinit var address: Address
}